namespace Vidly2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'19ce9922-bcff-4a63-a794-1e97656d95cd', N'guest@vidly.com', 0, N'ANVVVCsSMY2sdfmK1ClLTgioMSdUdga+9AWZr/ASCtpmww3eoW0O4QKEaYEPd8loZQ==', N'de92ead1-5652-41ce-ba6f-d9297b2950a1', NULL, 0, 0, NULL, 1, 0, N'syukry@domain.com')
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7cf148e4-be4f-40f5-a957-2cf1f5c54f54', N'admin@vidly.com', 0, N'ANOAZO5mgEOEpDzyElYynphcFbGoIz0kO6d5yOdUIyCu9ydlIFMQVjn4WNbny0rmTQ==', N'290679d4-8160-463d-9446-81a9dad43ac2', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
                    
                    INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'48408653-2356-4995-946e-83e49df92122', N'CanManageMovies')
                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7cf148e4-be4f-40f5-a957-2cf1f5c54f54', N'48408653-2356-4995-946e-83e49df92122')
            ");
        }
        
        public override void Down()
        {
        }
    }
}
