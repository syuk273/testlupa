﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly2.Models;
using Vidly2.ViewModels;

namespace Vidly2.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult New()
        {
            var membershipTypes = _context.MembershipTypes.ToList();
            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = membershipTypes
            };
            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            if(!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("CustomerForm", viewModel);
            }

            if(customer.ID == 0)
                _context.Customers.Add(customer);
            else
            {
                var customerInDb = _context.Customers.Single(c => c.ID == customer.ID);

                //TryUpdateModel(customerInDb);

                //Mapper.Map(customer, customerInDb);

                customerInDb.Name = customer.Name;
                customerInDb.Birthdate = customer.Birthdate;
                customerInDb.MembershipTypeId = customer.MembershipTypeId;
                customerInDb.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Customer");
        }

        public ViewResult  Index()
        {
            //Enable Memory Cache Reference first
            //if(MemoryCache.Default["Genres"] == null)
            //{
            //    MemoryCache.Default["Genres"] = _context.Genres.ToList();
            //}

            //var genres = MemoryCache.Default["Genres"] as IEnumerable<Genre>;

            return View();
        }

        public ActionResult Details(int id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.ID == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }

        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.ID == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = new CustomerFormViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };
            return View("CustomerForm", viewModel);
        }

        // GET: Customers
        //public ActionResult Index()
        //{
        //    //var customer = new Customer() { Name = "John" };
        //    //return View(customer);

        //    var customers = new List<Customer>
        //    {
        //        new Customer { Name = "John", ID = 1 },
        //        new Customer { Name = "Wick", ID = 2 }
        //    };

        //    var viewModel = new RandomMovieViewModel
        //    {
        //        Customers = customers
        //    };


        //    return View(viewModel);
        //}

        //public ActionResult Details(int id)
        //{
        //    if (id == 1)
        //    {
        //        var customers = new List<Customer>
        //        {
        //            new Customer { Name = "John", ID = id }
        //        };

        //        var viewModel = new RandomMovieViewModel
        //        {
        //            Customers = customers
        //        };
        //        return View(viewModel);
        //    }
        //    else if (id == 2)
        //    {
        //        var customers = new List<Customer>
        //        {
        //            new Customer { Name = "Wick", ID = id }
        //        };

        //        var viewModel = new RandomMovieViewModel
        //        {
        //            Customers = customers
        //        };
        //        return View(viewModel);
        //    }
        //    else
        //    {
        //        return HttpNotFound();
        //    }

        //    //return View(viewModel);
        //}
    }
}