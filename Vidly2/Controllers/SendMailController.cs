﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly2.Models;
using Vidly2.Helper;
using System.Data.Entity;

namespace Vidly2.Controllers
{
    public class SendMailController : Controller
    {
        private ApplicationDbContext _context;

        public SendMailController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: SendMail
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendEmailView()
        {
            var test = new SendMail();
            test.EmailSubject = MailHelper.SendMail1();

            return View(test);
        }

        public ActionResult SendEmailView2(SendMail sendMail)
        {
            var test = new SendMail();
            test.EmailSubject = MailHelper.SendMail2(sendMail);

            return View(test);
        }

        public ActionResult SendEmailView3()
        {
            var customer = _context.Customers.Include(c => c.MembershipType).ToList();
            var test = new Customer();
            string test2 = "";
            string test3 = "nurfazianas@gmail.com";

            foreach (var test1 in customer)
            {
                test2 = test2 + test1.Name + ",";
            }

            test2 = test2.Remove(test2.Length - 1);
            test.Name = MailHelper.SendMail3(test2, test3);

            return View(test);

        }
    }
}