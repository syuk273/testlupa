﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using Vidly2.Models;

namespace Vidly2.Helper
{
    public static class MailHelper
    {
        //hardcode
        public static string SendMail1()
        {
            var errorMessage = "";
            var state = "";

            try
            {
                //parameters to send email
                string ToEmail = "syukryismail@yahoo.com,syuk_lin@yahoo.com",
                    FromOrSenderEmail = "syukry273@gmail.com",
                    header = "RADIS-Mailer - RADIS AUTOMATED NOTIFIER Method 1",
                    SubJect, Body,
                    newLine = "<br/>",
                    sign = "RADIS Admin",
                    msgBorder = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
                    automated = "This is an automatically generated notification. Please do not reply to this email. We are unable to respond to inquiries sent to this address.",
                    thxMsg = "Thank you.";

                //Reading values from form collection (Querystring) and assigning values to parameters
                SubJect = "Test Notification for Method 1. " + header;
                Body = newLine + "Testing message for Method 1." +
                    newLine + newLine + thxMsg +
                    newLine + newLine + sign +
                    newLine + newLine + newLine +
                    newLine + msgBorder + newLine +
                    automated + newLine + msgBorder;

                //Configuring webMail class to send emails
                WebMail.SmtpServer = "smtp.gmail.com"; //gmail smtp server
                WebMail.SmtpPort = 587; //gmail port to send emails
                WebMail.EnableSsl = true; //sending emails with secure protocol
                WebMail.UserName = "syukry273";//EmailId used to send emails from application
                WebMail.Password = "lin241291";
                WebMail.From = FromOrSenderEmail; //email sender email address.

                //Sending email
                WebMail.Send(to: ToEmail, subject: SubJect, body: Body, isBodyHtml: true);

                state = "success";
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            if (errorMessage == "")
            {
                return state;
            }
            else
            {
                state = errorMessage;

                return state;
            }
        }

        //pass variable from form
        public static string SendMail2(SendMail sendMail)
        {
            var errorMessage = "";
            var state = "";

            try
            {
                //parameters to send email
                string ToEmail,
                    FromOrSenderEmail = "syukry273@gmail.com",
                    header = "RADIS-Mailer - RADIS AUTOMATED NOTIFIER Method 2",
                    SubJect, Body, cc, Bcc,
                    newLine = "<br/>",
                    sign = "RADIS Admin",
                    msgBorder = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
                    automated = "This is an automatically generated notification. Please do not reply to this email. We are unable to respond to inquiries sent to this address.",
                    thxMsg = "Thank you.";

                //Reading values from form collection (Querystring) and assigning values to parameters
                ToEmail = sendMail.ToEmail;
                SubJect = sendMail.EmailSubject + " " + header;
                Body = newLine + sendMail.EMailBody +
                    newLine + newLine + thxMsg +
                    newLine + newLine + sign +
                    newLine + newLine + newLine +
                    newLine + msgBorder + newLine +
                    automated + newLine + msgBorder;
                cc = sendMail.EmailCC;
                Bcc = sendMail.EmailBCC; ;

                //Configuring webMail class to send emails
                WebMail.SmtpServer = "smtp.gmail.com"; //gmail smtp server
                WebMail.SmtpPort = 587; //gmail port to send emails
                WebMail.EnableSsl = true; //sending emails with secure protocol
                WebMail.UserName = "syukry273";//EmailId used to send emails from application
                WebMail.Password = "lin241291";
                WebMail.From = FromOrSenderEmail; //email sender email address.

                //Sending email
                WebMail.Send(to: ToEmail, subject: SubJect, body: Body, cc: cc, bcc: Bcc, isBodyHtml: true);

                state = "success";
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            if (errorMessage == "")
            {
                return state;
            }
            else
            {
                state = errorMessage;

                return state;
            }
        }

        //passvariable from DB
        public static string SendMail3(string ToMail, string CarbonCopy)
        {
            var errorMessage = "";
            var state = "";

            try
            {
                //parameters to send email
                string ToEmail = ToMail,
                    cc = CarbonCopy,
                    FromOrSenderEmail = "syukry273@gmail.com",
                    header = "RADIS-Mailer - RADIS AUTOMATED NOTIFIER Method 3",
                    SubJect, Body,
                    newLine = "<br/>",
                    sign = "RADIS Admin",
                    msgBorder = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
                    automated = "This is an automatically generated notification. Please do not reply to this email. We are unable to respond to inquiries sent to this address.",
                    thxMsg = "Thank you.";

                //Reading values from form collection (Querystring) and assigning values to parameters
                SubJect = "Test Notification method 3. " + header;
                Body = newLine + "Testing message method 3." +
                    newLine + newLine + thxMsg +
                    newLine + newLine + sign +
                    newLine + newLine + newLine +
                    newLine + msgBorder + newLine +
                    automated + newLine + msgBorder;

                //Configuring webMail class to send emails
                WebMail.SmtpServer = "smtp.gmail.com"; //gmail smtp server
                WebMail.SmtpPort = 587; //gmail port to send emails
                WebMail.EnableSsl = true; //sending emails with secure protocol
                WebMail.UserName = "syukry273";//EmailId used to send emails from application
                WebMail.Password = "lin241291";
                WebMail.From = FromOrSenderEmail; //email sender email address.

                //Sending email
                WebMail.Send(to: ToEmail, subject: SubJect, body: Body, cc: cc, isBodyHtml: true);

                state = "success";
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            if (errorMessage == "")
            {
                return state;
            }
            else
            {
                state = errorMessage;

                return state;
            }
        }
    }
}